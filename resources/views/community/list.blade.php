<ul class="list-group">
	@if(!$links->isEmpty())
		@foreach($links as $link)
			<li class="list-group-item">

				<form method="POST" action="/votes/{{ $link->id }}">
					{{ csrf_field() }}

					<button class="btn {{ Auth::check() && Auth::user()->votedFor($link) ? 'btn-success' : 'btn-default' }}">
						{{ $link->votes->count() }}
					</button>

				</form>
				
				<a href="/community/{{ $link->channel->slug }}" class="label label-default" style="background: {{ $link->channel->color }}">
					{{ $link->channel->title }}
				</a>

				<a href="{{ $link->link }}" target="_blank">	
					{{ $link->title }}
				</a>

				<small>
					Contributed By: {{$link->user->name}} {{ $link->updated_at->diffForHumans() }}
				</small>
			</li>
		@endforeach
	@else
		<h5>No Posts Yet....</h5>
	@endif
</ul>

{{ $links->links() }}