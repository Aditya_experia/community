@if(Auth::check())
<div class="col-md-4">
	<h3>Contribute a link</h3>

	<div class="panel panel-default">
		<div class="panel-body">
			<form method="POST" action="{{ URL::to('/community') }}">

				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-group">
					<label for="channel">Channel:</label>

					<select class="form-control" name="channel_id">
						<option selected="disabled">Pick a Channel.....</option>

						@foreach($channels as $channel)
							<option value="{{ $channel->id }}" {{ old('channel_id') == $channel->id ? 'selected' : '' }}>
								{{ $channel->title }}
							</option>
						@endforeach
					</select>

					{{ $errors->first('channel_id') }}
				</div>

	
				<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
					<label for="title">Title:</label>

					<input type="text" class="form-control" id="title" placeholder="what is the name of your article ?" name="title" value="{{ old('title') }}" required>	
					{{ $errors->first('title') }}
				</div>

				<div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
					<label for="link">link:</label>

					<input type="text" class="form-control" id="link" placeholder="what is the URL" name="link" value="{{ old('link') }}" required>
					{{ $errors->first('link') }}	
				</div>

				<div class="form-group">
					<button class="btn btn-primary">Contribute Link</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endif