<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\CommunityLink;
use App\Channel;
use App\Exceptions\CommunityLinkAlreadySubmitted;
use App\Http\Requests\CommunityLinkForm;

class CommunityLinksController extends Controller
{
    public function index(Channel $channel = null)
    {	
        $links = CommunityLink::with('votes')->forChannel($channel)
                ->where('approved', 1)
                ->latest('updated_at')
                ->paginate(7);
                
    	$channels = Channel::orderBy('title', 'asc')->get();
        
    	return view('community.index', compact('links', 'channels', 'channel'));
    }


    public function store(CommunityLinkForm $form)
    {	
        try{
            $form->persist();

            if(Auth::user()->trusted){
                flash('Thanks for the contribution');
            }else{
                flash()->overlay('Thanks!', 'Your post will be updated shortly');
            }

        }catch(CommunityLinkAlreadySubmitted $e){

            flash()->overlay(
                            'We will instead bump the timestamps and bring that link to the top Thanks!', 
                            'That link has already been submitted'
            );
        }

    	return back();	
    }


}
