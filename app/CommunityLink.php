<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\CommunityLinkAlreadySubmitted;

class CommunityLink extends Model
{	

  	protected $fillable = [
  		'user_id', 'channel_id', 'title', 'link', 'approved'
  	];

    public function user($value='')
    {
    	return $this->belongsTo(User::class);
    }


   	public static function from(User $user)
   	{
   		$link = new static;

   		$link->user_id = $user->id;

      if($user->isTrusted()){

        $link->approve();

      }

   		return $link;
   	}

   	public function contribute($attributes)
   	{	  
      if($existing = $this->hasAlreadyBeenSubmitted($attributes['link'])){
        $existing->touch();

        throw new CommunityLinkAlreadySubmitted;
      }
   		return $this->fill($attributes)->save();
   	}

    public function channel()
    {
        return $this->belongsTo(Channel::class); 
    }


    public function approve()
    {
        $this->approved = true;

        return $this;
    }

    public function hasAlreadyBeenSubmitted($link)
    {
        return static::where('link', $link)->first();
    }

    /**
     * dynamic query scope for index method in community link controller
     * @param  collection  
     * @param  $channel
     * @return query       
     */
    public function scopeForChannel($query, $channel)
    {   
        if($channel->exists){
          $query->where('channel_id', $channel->id);
        }
        return $query;
    }


    /**
     * community link hasMany votes.
     * @return communityLink
     */
    public function votes()
    {
      return $this->hasMany(CommunityLinkVote::class, 'community_link_id');
    }
}
